;; hide toolbar
(tool-bar-mode -1)

;; hide menu-bar
(menu-bar-mode -1)

;; start window size
(add-to-list 'default-frame-alist '(width . 130))
(add-to-list 'default-frame-alist '(height . 40))

;; inhibit startup/splash screen
(setq inhibit-splash-screen t)
(setq ingibit-startup-message t)

;; enable whitespace-mode
(global-whitespace-mode 1)
