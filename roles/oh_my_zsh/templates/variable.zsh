# default theme
ZSH_THEME="miloshadzic"

# display red dots whilst waiting for completion
COMPLETION_WAITING_DOTS="true"

# load plugins
plugins=(git npm pip zsh-autosuggestions)
