# media storage
export STORAGE="{{ user_storage }}"

# improve output of less
export LESS="--tabs=2 --status-column --ignore-case --LONG-PROMPT --RAW-CONTROL-CHARS --LINE-NUMBERS --HILITE-UNREAD"
